﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AAC.Models;
using System.Drawing;
using System.IO;
using System.Web;

namespace AAC.Controllers.api
{
    public class MatchDayController : ApiController
    {
        [HttpGet]
        [Route("api/matchday")]

        public System.Web.Http.Results.JsonResult<MatchDay> Get()
        {
            const string basePetsPath = "~/Content/ImageRepository/pets/";
            const string baseTeamsPath = "~/Content/ImageRepository/teams/";

            return Json(new MatchDay
            {
                MatchDayId = 321,
                MatchDayName = "Session 1",
                RoundId = 4745,
                RoundName = "Semi-Final",
                CompetitionTypeId = 1,
                CompetitionId = 96,
                CompetitionTypeName = "Mastiff League",
                Season = 2016,
                Matches = new List<Match>() {
                    new Match() {
                        HomeTeam = new Team() {
                            Name = "Bone Intruders",
                            Image = ImageToBase64(baseTeamsPath + "rsz_images2.png"),
                            SquadList = new List<Puppy>()
                            {
                                new Puppy() {
                                    PuppyId = 16,
                                    Name = "Blue Tornado",

                                    Image = ImageToBase64(basePetsPath + "rsz_1.png") //alt+0126
                                },
                                new Puppy() {
                                    PuppyId = 1,
                                    Name = "Nun Bes",
                                    Image = ImageToBase64(basePetsPath + "rsz_2.png") 
                                },
                                new Puppy() {
                                    PuppyId = 2,
                                    Name = "Peep Juvenalis",
                                    Image = ImageToBase64(basePetsPath + "rsz_3.png") 
                                },
                                new Puppy() {
                                    PuppyId = 3,
                                    Name = "Android Punch",
                                    Image = ImageToBase64(basePetsPath + "rsz_4.png") 
                                },
                            },
                            LineUp = new List<PuppyRelation>() {
                                    new PuppyRelation() { PuppyId = 16},
                                    new PuppyRelation() { PuppyId = 1},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 3},
                                    new PuppyRelation() { PuppyId = 0},

                            },
                        },
                        AwayTeam = new Team() {
                            Name = "Cyrenaic Pirates",
                            Image = ImageToBase64(baseTeamsPath + "rsz_images5.png"),

                            SquadList = new List<Puppy>()
                            {
                                new Puppy() {
                                    PuppyId = 4,
                                    Name = "BoomBoom Chop",
                                    Image = ImageToBase64(basePetsPath + "rsz_9.png") //alt+0126,
                                },
                                new Puppy() {
                                    PuppyId = 5,
                                    Name = "Rush Hour",
                                    Image = ImageToBase64(basePetsPath + "rsz_10.png") 
                                },
                                new Puppy() {
                                    PuppyId = 6,
                                    Name = "Four Season",
                                    Image = ImageToBase64(basePetsPath + "rsz_11.png") 
                                },
                                new Puppy() {
                                    PuppyId = 7,
                                    Name = "Ricochet",
                                    Image = ImageToBase64(basePetsPath + "rsz_12.png") 
                                },
                            },
                            LineUp = new List<PuppyRelation>() {
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 4},
                                    new PuppyRelation() { PuppyId = 6},
                                    new PuppyRelation() { PuppyId = 5},
                                    new PuppyRelation() { PuppyId = 0},

                            },
                        },
                    },
                    new Match() {
                        HomeTeam = new Team() {
                            Name = "Silver Chasers",
                            Image = ImageToBase64(baseTeamsPath + "rsz_images3.png"),
                            SquadList = new List<Puppy>()
                            {
                                new Puppy() {
                                    PuppyId = 8,
                                    Name = "UmpaLumpa",
                                    Image = ImageToBase64(basePetsPath + "rsz_1.png") //alt+0126
                                },
                                new Puppy() {
                                    PuppyId = 9,
                                    Name = "Calico",
                                    Image = ImageToBase64(basePetsPath + "rsz_2.png")
                                },
                                new Puppy() {
                                    PuppyId = 10,
                                    Name = "Bruno",
                                    Image = ImageToBase64(basePetsPath + "rsz_3.png")
                                },
                                new Puppy() {
                                    PuppyId = 11,
                                    Name = "Mortimer",
                                    Image = ImageToBase64(basePetsPath + "rsz_4.png")
                                },
                            },
                            LineUp = new List<PuppyRelation>() {
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},

                            },
                        },
                        AwayTeam = new Team() {
                            Name = "Gecko Dragons",
                            Image = ImageToBase64(baseTeamsPath + "rsz_images4.png"),
                            SquadList = new List<Puppy>()
                            {
                                new Puppy() {
                                    PuppyId = 12,
                                    Name = "Marlboro",
                                    Image = ImageToBase64(basePetsPath + "rsz_9.png") //alt+0126,
                                },
                                new Puppy() {
                                    PuppyId = 13,
                                    Name = "Brutus",
                                    Image = ImageToBase64(basePetsPath + "rsz_10.png")
                                },
                                new Puppy() {
                                    PuppyId = 14,
                                    Name = "Jezebel",
                                    Image = ImageToBase64(basePetsPath + "rsz_11.png")
                                },
                                new Puppy() {
                                    PuppyId = 15,
                                    Name = "Zuni",
                                    Image = ImageToBase64(basePetsPath + "rsz_12.png")
                                },
                            },
                            LineUp = new List<PuppyRelation>() {
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},
                                    new PuppyRelation() { PuppyId = 0},

                            },
                        },
                    },
                },

            });
        }

        private string ImageToBase64(string path)
        {
            var image = Image.FromFile(HttpContext.Current.Server.MapPath(path));
            using (MemoryStream ms = new MemoryStream())
            {
                //image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                //byte[] imageBytes = ms.ToArray();

                //string base64String = Convert.ToBase64String(imageBytes);
                //return base64String;

                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        #region

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        } 
        #endregion
    }
}