﻿var app = angular.module('app', [
    'ngRoute',
    'ng-appcache',
    'ngAnimate',
    'angular-loading-bar',
    'ui.bootstrap',
    'indexedDB'
])
.config(function ($indexedDBProvider, $httpProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $indexedDBProvider
      .connection('mDB')
      .upgradeDatabase(1, function (event, db, tx) {
          var objStore = db.createObjectStore('mdays', { keyPath: 'MatchDayId' });
      })
})
.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.latencyThreshold = 0;
}])
.controller('appController',
    ['$scope', '$interval', '$animate', 'appcache', '$http', '$indexedDB', 'apiService', '$uibModal', '$log', function ($scope, $interval, $animate, appcache, $http, $indexedDB, apiService, $uibModal, $log) {
        appcache.checkUpdate().then(function () {
            appcache.swapCache().then(function () {
                location.reload(true);

            });

        });
        $scope.eApplicationStatus = {
            Indeterminate: { Name: 'indeterminate', Value: 0 },
            OnLine: { Name: 'online', Value: 1 },
            OffLine: { Name: 'offline', Value: 2 }
        }
        $scope.matchday = {};
        $scope.selectedMember = {};
        $scope.selectedTeam = {};

        $scope.Status = $scope.eApplicationStatus.Indeterminate;

        $scope.LoadFromLocalStore = function () {
            $indexedDB.openStore('mdays', function (store) {
                store.getAll().then(function (mday) {
                    $scope.matchday = mday[0];
                });
            });
        }

        $scope.setApplication = function (data) {
            $indexedDB.openStore('mdays', function (store) {
                store.clear().then(function () {
                    store.insert(data).then(function (e) {
                        store.getAll().then(function (mday) {
                            $scope.matchday = mday[0];
                        });

                    });
                });
            });
        };

        $scope.openSelectMember = function (size, team, member) {
            $scope.selectedTeam = team;
            $scope.selectedMember = member;

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'squadMembersModal',
                controller: 'appController',
                size: size,
                scope: $scope,
                backdrop: 'static',
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

        $scope.getPuppyImage = function (team, puppy) {
            var result = "";
            var id = puppy.PuppyId;
            if (id > 0) {
                var pup = _.filter(team.SquadList, function (item) {
                    return item.PuppyId == id;
                });
                if (!_.isEmpty(pup)) {
                    result += _.first(pup).Image;

                }

            }

            return result;
        }

        $scope.getPuppyName = function (team, puppy) {
            var result = "";
            var id = puppy.PuppyId;

            if (id > 0) {
                var pup = _.filter(team.SquadList, function (item) {
                    return item.PuppyId == id;
                });
                if (!_.isEmpty(pup)) {
                    result += _.first(pup).Name;

                }

            }

            return result;

        };

        $scope.select = function () {

            this.$close();

        }
        $scope.cancel = function () {
            this.$dismiss('cancel');

        }

        function init() {
            var matchdayPromise = apiService.getMatchday();

            matchdayPromise.success(function (response) {
                $scope.setApplication(response);
                $scope.Status = $scope.eApplicationStatus.OnLine;
                document.getElementById('dizzy').play();

            });
            matchdayPromise.error(function (response) {
                $scope.LoadFromLocalStore();
                $scope.Status = $scope.eApplicationStatus.OffLine;
                document.getElementById('dizzy').play();
            });
        }

        init();
    }]);
