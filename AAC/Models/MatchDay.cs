﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAC.Models
{
    public class MatchDay
    {
        public int CompetitionId { get; internal set; }
        public int CompetitionTypeId { get; internal set; }
        public string CompetitionTypeName { get; internal set; }
        public int MatchDayId { get; internal set; }
        public string MatchDayName { get; internal set; }
        public string Name { get; set; }
        public int RoundId { get; internal set; }
        public string RoundName { get; internal set; }
        public int Season { get; internal set; }
        public List<Match> Matches { get; set; }
    }

    public class Match
    {
        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }

    }

    public class Team
    {
        public string Name { get; set; }
        public List<Puppy> SquadList { get; set; }
        public List<PuppyRelation> LineUp { get; set; }
        public string Image { get; internal set; }
    }

    public class Puppy
    {
        public int PuppyId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class PuppyRelation
    {
        public Puppy Puppy { get; set; }
        public int PuppyId { get; set; }

    }
}