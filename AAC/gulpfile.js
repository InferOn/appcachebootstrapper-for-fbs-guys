﻿/// <binding BeforeBuild='compile-less' AfterBuild='js-fef, update-manifest-version, css-fef' />

var gulp = require('gulp'),
    rimraf = require("gulp-rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    less = require("gulp-less"),
    rename = require("gulp-rename"),
    change = require("gulp-change"),
    watch = require("gulp-watch");

function updateManifestVersion(content) {
    var options = {
        weekday: "long", year: "numeric", month: "short",
        day: "numeric", hour: "2-digit", minute: "2-digit"
    };
    var today = new Date();
    return content.replace(/^(.*)$/m, 'CACHE MANIFEST # '
                                        + today.toLocaleDateString("en-us", options)
                                        + today.getHours()
                                        + ":" + today.getMinutes()
                                        + ":" + today.getSeconds()
                                        + ":" + today.getMilliseconds()
                                        + ' # Explicitly cached ');
}

gulp.task('compile-less', function () {
    gulp.src('./Content/less/app.less')
    .pipe(less())
    .pipe(gulp.dest('./Content/'));

});

gulp.task('update-manifest-version', function () {
    gulp.src('*.manifest', { read: false })
        .pipe(rimraf());

    gulp.src('AppCacheSource/appcache.manifest.source')
        .pipe(change(updateManifestVersion))
        .pipe(rename("appcache.manifest"))
        .pipe(gulp.dest('./'))

});

gulp.task('js-fef', function () {
    return gulp.src(['Scripts/jquery-1.9.1.min.js',
                     'Scripts/lodash.js',
                     'Scripts/lodash.js',
                     'Scripts/bootstrap.min.js',
                     'Scripts/angular.min.js',
                     'Scripts/angular-animate.min.js',
                     'Scripts/loading-bar.min.js',
                     'Scripts/ui-bootstrap-tpls-1.3.1.js',
                     'Scripts/angular-route.min.js',
                     'Scripts/indexed-db.js',
                     'Scripts/appcache.min.js',
                     'App/app.js',
                     'App/apiservice.js'
    ])
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./'));
});

//gulp.task('css-fef', function () {
//    return gulp.src(['Content/bootstrap.min.css',
//                     'Content/bootstrap-theme.min.css',
//                     'Content/loading-bar.css',
//                     'Content/app.css'])
//        .pipe(concat('style.css'))
//        .pipe(uglify())
//        .pipe(gulp.dest('./'));
//});

