# README #

### What is this repository for? ###

Simple prototype of an off-line capable web-application. 

It contains:
 
 * a simple MVC back-end API to get a minimal viable set of data.  
 * a simple Angular 1.x application which use AppCache and IndexedDB to manage data across network availability.
 * a Gulp configuration to manage a pre-build task for .less trasformation of stilesheet files, a post-build task to concat css and js files, and a post-build task to manage app-cache manifest dynamic versioning.

![Immagine.png](https://bitbucket.org/repo/78ARa8/images/4206190947-Immagine.png)

### How do I get set up? ###

* Clone it on your desktop
* Open with Visual Studio
* F5